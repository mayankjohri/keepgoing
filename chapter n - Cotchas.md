# Chapter n: Go-tcha's
_ My mistakes while learning and after _

This chapter contains many common mistakes i have made while learning Go. Few I have found on internet and tried to club them in a single place for my future reference. 


## Imports
- Unused Imports

## Variables 
- Unused Variables
- Redeclaring Variables Using Short Variable Declarations
- Accidental Variable Shadowing
- Can't Use Short Variable Declarations to Set Field Values
- Short Variable Declarations Can Be Used Only Inside Functions
- Can't Use "nil" to Initialize a Variable Without an Explicit Type

## Slice and Maps
- Map Capacity
- Using "nil" Slices and Maps
- Opening Brace Can't Be Placed on other/next Line
- Accessing Non-Existing Map Keys
- Updating and Referencing Item Values in Slice, Array, and Map "for range" Clauses
- "Hidden" Data in Slices
- Slice Data Corruption
- "Stale" Slices
 

## Arrays 
- Array Function Arguments
- Unexpected Values in Slice and Array "range" Clauses
- Slices and Arrays Are One-Dimensional

## Strings 
- Strings Can't Be "nil"
- Strings Are Immutable
- Conversions Between Strings and Byte Slices
- Strings and Index Operator
- Strings Are Not Always UTF8 Text
- String Length
- Missing Comma In Multi-Line Slice/Array/Map Literals

## Data Structures
- Built-in Data Structure Operations Are Not Synchronized
- Unexported Structure Fields Are Not Encoded


## logging 
- log.Fatal and log.Panic Do More Than Log


## Iterations 
- Increments and Decrements
- Fallthrough Behavior in "switch" Statements
- Iteration Values For Strings in "range" Clauses
- Iterating Through a Map Using a "for range" Clause
- Iteration Variables and Closures in "for" Statements
- Breaking Out of "for switch" and "for select" Code Blocks

## Operators
- Bitwise NOT Operator
- Operator Precedence Differences

- App Exits With Active Goroutines

## Channel
- Sending to an Unbuffered Channel Returns As Soon As the Target Receiver Is Ready
- Sending to an Closed Channel Causes a Panic
- Using "nil" Channels
- Methods with Value Receivers Can't Change the Original Value

## Web
- Closing HTTP Response Body
- Closing HTTP Connections

## JSON
- Unmarshalling JSON Numbers into Interface Values

## Error Handling
- Comparing Structs, Arrays, Slices, and Maps
- Recovering From a Panic


- Type Declarations and Methods
- Deferred Function Call Argument EvaluationDeferred Function Call Execution
- Failed Type Assertions
- Blocked Goroutines and Resource Leaks

## Advanced Beginner:

Using Pointer Receiver Methods On Value Instances
Updating Map Value Fields
"nil" Interfaces and "nil" Interfaces Values
Stack and Heap Variables
GOMAXPROCS, Concurrency, and Parallelism
Read and Write Operation Reordering
Preemptive Scheduling 