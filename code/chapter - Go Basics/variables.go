package main

import "fmt"

func main() {
	var hello string
	space := " "
	hello = "Hello"
	var world = "World"
	fmt.Println(hello + space + world)
	var val = "String"
	testingString := "Testing with " + val
	fmt.Println(testingString)
	one := 1
	fmt.Println("Sum of 1 + 1 = ", one+one)
	var twoThree int
	twoThree = 23
	fmt.Println("Multiplication of 1.22 & 23 = ", 2*twoThree)
}
