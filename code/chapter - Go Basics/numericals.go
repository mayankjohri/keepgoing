package main

import "fmt"

func main() {
	var test int
	var test1 int32
	var test2 byte
	var test3 uint8

	test = 12311
	test1 = 12312311
	test2 = 255 // 256 will result in error
	test3 = 10
	fmt.Println(test)
	fmt.Println(test1)
	fmt.Println(test2)
	// fmt.Println(test + test1) -> This will produce error
	// invalid operation: test + test1 (mismatched types int and int32)
	fmt.Println(test2 - test3)
}
