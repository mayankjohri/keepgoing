package main

import "fmt"

func main() {
	const (
		Pi        = 3.1417
		c         = 299792458
		TrueFlag  = true
		FalseFlag = !TrueFlag
		a         = -11.12
	)
	fmt.Println(TrueFlag)
	fmt.Println(Pi * c)
	fmt.Println(a * (-Pi))
}
