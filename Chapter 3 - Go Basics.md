{:: encoding="utf-8" /}
# Go Basics

Go is a compiled language, that means that Go source code and all its dependencies re converted into native machine language.

## IDE
Many IDE's supports Go langauge either nativly or through either extensions. The detailed list can be found in [Golang github website](https://github.com/golang/go/wiki/IDEsAndTextEditorPlugins "").

I personally prefer "Atom" IDE with its various plugins. Another one which I like is [IntelliJ Go](http://go-ide.com).

## Executing the code
The Go code can be executed by running the following command or else use `<Shift><Ctrl>b` in Atom with "Script" plugin installed.
```bash
go run <filename.go>
```
Go is a tool for managing Go source code.

Usage:

	go command [arguments]

The commands are:

	build       compile packages and dependencies
	clean       remove object files
	doc         show documentation for package or symbol
	env         print Go environment information
	fix         run go tool fix on packages
	fmt         run gofmt on package sources
	generate    generate Go files by processing source
	get         download and install packages and dependencies
	install     compile and install packages and dependencies
	list        list packages
	run         compile and run Go program
	test        test packages
	tool        run specified go tool
	version     print Go version
	vet         run go tool vet on packages

## Hello World
Lets start with the most common program in the world of learning "Hello World" :)


```go
package main
import "fmt"

func main() {
	fmt.Println("Hello, नमस्ते, आप कैसे हैं?")
}

```
save the above code in `hello.go` file. In command prompt navigate to the folder where this file is saved run the following command `go run hello.go`.

Execution Result:
```go
Hello, नमस्ते, आप कैसे हैं?
```

Lets understand the above program

### packages
All Go programs are made of **packages**. Program start running in package `main`.

> By convention, the package name is the same as the last element of the import path. For instance, the "math/rand" package comprises files that begin with the statement package rand.

### import
**import** is used to import various packages in the program, as shown in the example above.


```go
import "fmt"
import "os"
import "sys"
```
or multiple imports can be clubbed as shown in the below example,


```go
import(
  "fmt"
  "os"
  "sys
}

```

### Exported names
In Go, the function is exported if it begans with capital letter as in the example above,
`fmt.Println`
i.e. if package fmt contains a name `println` than it can't be called by other packages.

### Functions
Function is a collection of instructions which are clubbed in a block and can be called using a defined name. Depending on the type of function it can be either called from within the package or outside.

Function can take zero or more arguments

```go
package main

import "fmt"

func sum(x int, y int) int {
	return x + y
}

func printHello(){
    fmt.Println("Hello !")
}

func main() {
	fmt.Println(sum(10, 122))
}
```


## Variables - Names & Values (datatype)

In Go the variable are explicitly declared. Main difference between common langauges and Go is that data type is listed after the variable type as shown in the syntex below
```go
var <variable_name> <data_type>
```
Examples
```go
var testResult bool
var apples int
```


More Examples: Using them in real world
```go
package main

import "fmt"

func main() {
	var hello string
	space := " "
	hello = "Hello"
	var world = "World"
	fmt.Println(hello + space + world)
	var val = "String"
	testingString := "Testing with " + val
	fmt.Println(testingString)
	one := 1
	fmt.Println("Sum of 1 + 1 = ", one+one)
	var twoThree int
	twoThree = 23
	fmt.Println("Multiplication of 1.22 & 23 = ", 2*twoThree)
}

```

`:=` can be used as shorthand for both declare and initialization of the variable as shown in the examples below

```go
package main

import "fmt"

func main() {
	var hello string
	space := " "
	hello = "Hello"
	var world = "World"
	fmt.Println(hello + space + world)
	var val = "String"
	testingString := "Testing with " + val
	fmt.Println(testingString)
	one := 1
	fmt.Println("Sum of 1 + 1 = ", one+one)
	var twoThree int
	twoThree = 23
	fmt.Println("Multiplication of 1.22 & 23 = ", 2*twoThree)
}

```
Output:-
```
Hello World
Testing with String
Sum of 1 + 1 =  2
Multiplication of 1.22 & 23 =  46
```

Multiple varialbes can be declared using the following method also.
***Syntax***
```go
var (
	<name> datatype
    <name> = "data>
)
```
**Example**
```go
package main

import "fmt"

func main() {
	var (
		myCount int
		name    = "Mayank Johri"
		flg     bool
	)
	fmt.Println(myCount, name, flg)
}

```
variables can be initialized in sinle line also as shown below
```go
var (
	x, y, z = 1, "test", true
)
```
or, like this
```go
x, y, z := 1, "test", true
```

## Constants
Costants are declared similar to variables except `const` keyword is used instead of `var`.
They can only be either numeric value, character, string or boolean. They can't be declared using `:=`.

Examples:
```go
const Pi = 3.1417
const C = 299792458
```
```go
package main

import "fmt"

func main() {
	const (
		Pi        = 3.1417
		c         = 299792458
		TrueFlag  = true
		FalseFlag = !TrueFlag
	)
	fmt.Println(TrueFlag)
	fmt.Println(Pi * c)
}
```

### Data Type
Go has various primary data types including strings, integers, floats, booleans, etc to complex once such as arrays, maps, slices etc.

Here are a few basic examples.

```go
package main

import (
	"fmt"
)

func main() {
	fmt.Println("Hello " + "World")
	var val = "String"
	fmt.Println("Testing with " + val)
	fmt.Println("Sum of 1 + 1 = ", 1+1)
	fmt.Println("Multiplication of 1.22 & 23 = ", 1.22*23)
	fmt.Println("`true && false` is ", true && false)
	fmt.Println("true || false = ", true || false)
	fmt.Println("1 > 2 = ", 1 > 2)
	fmt.Println("!true = ", !true)
}

```

**Output**

```
Hello World
Testing with String
Sum of 1 + 1 =  2
Multiplication of 1.22 & 23 =  28.06
`true && false` is  false
true || false =  true
1 > 2 =  false
!true =  false
```

In Go, variables declared **without initialization** are **zero-valued** as shown in the example below

```go
	var myString string
	fmt.Println("#" + myString + "#")
	var myCount int
	fmt.Println(myCount)
```

Output:

```
##
0
```
In the above example `myString` contains no value and `myCount` contains `0`.

### Boolean
They represents the set of boolean values denoted by predefined keywords `true` and `false`. The boolean type is defined using the keyword `bool`.

Example:

```go
var myFlag bool
var trueFlag = true
falseFlag := false
```

### Numerical Types
#### Int
In the Go language, integer types are devided into signed integer and unsigned integer, then they are in accordance with the length of each divided into four types.

Signed integer: int8 int16 int32 int64

Unsigned integer: uint8 uint16 uint32 uint 64

The `int` takes the length depending on your processor. It means that on a `32bit` machine its size is `32bit`, and on `64bit` its `64bits`.


```go
package main

import "fmt"

func main() {
	var test int
	var test1 int32
	var test2 byte
	var test3 uint8

	test = 12311
	test1 = 12
	test2 = 255 // 256 will result in error
	test3 = 10
	fmt.Println(test)
	fmt.Println(test1)
	fmt.Println(test2)
	// fmt.Println(test + test1) -> This will produce error
	// invalid operation: test + test1 (mismatched types int and int32)
	fmt.Println(test2 - test3)
    }
```

**Output**:

    12311
    12312311
    255
    245

#### Floats
In the Go language, floats and float64 float32 into two types of IEEE 754 compliance.

Only the integer part of the float, to add a decimal point identifier, such as floating-point marker 1 1.0

For integer literals and floating-point literals, Go to use depending on the occasion, will be automatically integer -> floating-point or floating-point number -> integer conversion


#### Complex

Very seldom used, brief introduction

In the Go language, the complex is divided into complex64 and complex128, you can use the format 0 + 1i


### String
Strings are declared in Go using `string` keyword. It is a **read-only** slice of bytes. It can contains arbitrary bytes, which include binary data as well. So in short, it can contain any byte.

The `read-only` means that everything you change the string it create a new copy of the data and old one is discarded.


In the Go language, strings can be represented in two ways, one is the double quotation mark (") enclosed together, it may contain an escape character. Another is to use backquote (`) enclosed, can contain newline and other formatting characters, feeling a bit like heredoc other languages.

Since the minimum unit is composed of a string of characters, the smallest unit of storage is the byte string for traverse, until the whole is less than one-byte characters (that is, a value less than 256 yards) consisting of, you can walk or turn into a byte traverse sections, and comprising more than one byte characters, the safest approach is to convert rune slices.

